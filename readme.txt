SET OPERATIONS IN COLDFUSION
----------------------------

Background:
I needed to perform a set difference in ColdFusion.
I searched Google, didn't find anything, and decided 
it was an opportunity to use a couple things that are
not new, but new-to-me:
- MXUnit tests
- cfinterface

Currently it includes arraySet, an array-based implementation.

Operations included:
- add
- remove
- isMember
- union
- intersection
- relativeComplement (i.e. set difference)
- isSubset
- equals
- size

It is by no means perfect, but you may find it useful.