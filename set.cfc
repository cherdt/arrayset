<!--- interface defining basic required methods of any set implementations --->
<cfinterface>

	<cffunction name="add" access="public" output="no" returntype="void" hint="">
		<cfargument name="item" type="any" required="yes">
	</cffunction>

	<cffunction name="remove" access="public" output="no" returntype="void" hint="">
		<cfargument name="item" type="any" required="yes">
	</cffunction>

	<cffunction name="isMember" access="public" output="no" returntype="boolean" hint="">
		<cfargument name="item" type="any" required="yes">
	</cffunction>

	<cffunction name="size" access="public" output="no" returntype="Numeric" hint="">
	</cffunction>

	<cffunction name="union" access="public" output="no" returntype="set" hint="">
		<cfargument name="B" type="set" required="yes">
	</cffunction>

	<cffunction name="intersection" access="public" output="no" returntype="set" hint="">
		<cfargument name="B" type="set" required="yes">
	</cffunction>

	<cffunction name="relativeComplement" access="public" output="no" returntype="set" hint="">
		<cfargument name="B" type="set" required="yes">
	</cffunction>

	<cffunction name="isSubset" access="public" output="no" returntype="boolean" hint="">
		<cfargument name="B" type="set" required="yes">
	</cffunction>
	
	<cffunction name="equals" access="public" output="no" returntype="boolean" hint="">
		<cfargument name="B" type="set" required="yes">
	</cffunction>

</cfinterface>