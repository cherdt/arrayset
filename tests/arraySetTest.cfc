
<cfcomponent extends="mxunit.framework.TestCase">

	<!--- this will run before every single test in this test case --->
	<cffunction name="setUp" returntype="void" access="public" hint="put things here that you want to run before each test">
		<cfset A = createObject("component","arraySet.arraySet").init()>
		<cfset B = createObject("component","arraySet.arraySet").init()>
		<cfset C = createObject("component","arraySet.arraySet").init()>		
	</cffunction>

	<!--- this will run after every single test in this test case --->
	<cffunction name="tearDown" returntype="void" access="public" hint="put things here that you want to run after each test">
	</cffunction>

    
	<!--- this will run once after initialization and before setUp() --->
	<cffunction name="beforeTests" returntype="void" access="public" hint="put things here that you want to run before all tests">
	</cffunction>


	<!--- this will run once after all tests have been run --->
	<cffunction name="afterTests" returntype="void" access="public" hint="put things here that you want to run after all tests">
	</cffunction>


	<cffunction name="test_add" returntype="void" access="public">
		<cfscript>
			assert(NOT A.isMember("test"));
			A.add("test");
			assert(A.isMember("test"));
		</cfscript>
	</cffunction>


	<cffunction name="test_is_member" returntype="void" access="public">
		<cfscript>
			A.add("the");
			A.add("quick");
			A.add("brown");
			A.add("fox");
			
			assert(A.isMember("quick"));
			assert(NOT A.isMember("cow"));
		</cfscript>
	</cffunction>


	<cffunction name="test_union" returntype="void" access="public">
		<cfscript>
			A.add(1);
			A.add(2);
			A.add(3);			

			B.add(4);
			B.add(5);
			B.add(6);			

			C.add(1);
			C.add(2);
			C.add(3);
			C.add(4);
			C.add(5);
			C.add(6);			

			result = A.union(B);
			debug(result);

			assertEquals(result.members, C.members, "Expected union to contain 1,2,3,4,5,6");
		</cfscript>
	</cffunction>


	<cffunction name="test_intersection" returntype="void" access="public">
		<cfscript>
			A.add("the");
			A.add("quick");
			A.add("brown");
			A.add("fox");

			B.add("how");
			B.add("now");
			B.add("brown");
			B.add("cow");
			
			result = A.intersection(B);
			
			assert(result.isMember("brown"));
			assert(result.size() EQ 1);
			
		</cfscript>
	</cffunction>


	<cffunction name="test_relative_complement" returntype="void" access="public">
		<cfscript>
			A.add(1);
			A.add(2);
			A.add(3);			

			B.add(2);
			
			result = A.relativeComplement(B);
			assert(result.size() EQ 2);
			assert(result.isMember(1) AND result.isMember(3) AND NOT result.isMember(2));

			result = A.relativeComplement(A);
			assert(result.size() EQ 0);
		</cfscript>
	</cffunction>


	<cffunction name="test_is_subset" returntype="void" access="public">
		<cfscript>
			A.add(1);
			A.add(2);
			A.add(3);			

			B.add(2);
			
			assert(A.isSubset(B));
			assert(NOT B.isSubset(A));
		</cfscript>
	</cffunction>


	<cffunction name="test_equals" returntype="void" access="public">
		<cfscript>
			A.add(1);
			A.add(2);
			A.add(3);			

			B.add(2);

			C.add(1);
			C.add(2);
			C.add(3);			

			assert(NOT A.equals(B));
			assert(A.equals(A));
			assert(A.equals(C));
		</cfscript>
	</cffunction>

</cfcomponent>

