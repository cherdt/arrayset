<!--- An array-based implementation of a set --->
<cfcomponent name="arraySet" implements="set" output="false">

	<!--- init optionally takes an input array to populate the set --->
	<cffunction name="init" access="public" output="no" returntype="arraySet">
		<cfargument name="members" type="array" required="no">
		<cfset this.members = ArrayNew(1)>
		<cfif structKeyExists(arguments,"members")>
			<cfloop array="#arguments.members#" index="member">
				<cfscript>
					add(member);
				</cfscript>
			</cfloop>
		</cfif>
		<cfreturn this>
	</cffunction>
	
	<!--- Add an item to the set --->
	<cffunction name="add" access="public" output="no" returntype="void">
		<cfargument name="item" type="any" required="yes">
		<cfif NOT arrayContains(this.members, arguments.item)>
			<cfscript>
				arrayAppend(this.members, arguments.item);
			</cfscript>			
		</cfif>
	</cffunction>

	<!--- Remove an item from the set --->
	<cffunction name="remove" access="public" output="no" returntype="void">
		<cfargument name="item" type="any" required="yes">
		<cfif arrayContains(this.members, arguments.item)>
			<cfscript>
				arrayDelete(this.members, arguments.item);
			</cfscript>			
		</cfif>
	</cffunction>

	<!--- Check for membership in a set --->
	<cffunction name="isMember" access="public" output="no" returntype="boolean">
		<cfargument name="item" type="any" required="yes">
		<cfreturn arrayContains(this.members, arguments.item)>
	</cffunction>

	<!--- Return the size (cardinality) of the set --->
	<cffunction name="size" access="public" output="no" returntype="Numeric" hint="">
		<cfreturn ArrayLen(this.members)>
	</cffunction>

	<!--- Return the union of this set with set B --->
	<cffunction name="union" access="public" output="no" returntype="set">
		<cfargument name="B" type="set" required="yes">
		<cfset var myUnion = createObject("component","arraySet").init()>
		<cfloop array="#this.members#" index="member">
			<cfscript>
				myUnion.add(member);
			</cfscript>
		</cfloop>
		<!--- TODO: implement an iterator? this breaks encapsulation and relies on implementation --->
		<cfloop array="#arguments.B.members#" index="item">
			<cfscript>
				myUnion.add(item);
			</cfscript>
		</cfloop>
		<cfreturn myUnion>
	</cffunction>
	
	<!--- Return the intersection of this set with set B --->
	<cffunction name="intersection" access="public" output="no" returntype="set">
		<cfargument name="B" type="set" required="yes">
		<cfset var myIntersection = createObject("component","arraySet").init()>
		
		<cfloop array="#this.members#" index="member">
			<cfif B.isMember(member)>
				<cfscript>
					myIntersection.add(member);
				</cfscript>
			</cfif>
		</cfloop>
		<!--- TODO: implement an iterator? this breaks encapsulation and relies on implementation --->
		<cfloop array="#arguments.B.members#" index="item">
			<cfif isMember(item)>
				<cfscript>
					myIntersection.add(item);
				</cfscript>
			</cfif>
		</cfloop>
		<cfreturn myIntersection>
	</cffunction>

	<!--- Return the complement of B in this set, AKA set difference (this set - B) --->
	<cffunction name="relativeComplement" access="public" output="no" returntype="set">
		<cfargument name="B" type="set" required="yes">
		<cfset var complement = createObject("component","arraySet").init()>
		<cfloop array="#this.members#" index="member">
			<cfif NOT B.isMember(member)>
				<cfscript>
					complement.add(member);
				</cfscript>
			</cfif>
		</cfloop>
		<cfreturn complement>
	</cffunction>

	<!--- Return true if B is a subset of this set --->
	<cffunction name="isSubset" access="public" output="no" returntype="boolean">
		<cfargument name="B" type="set" required="yes">
		<!--- TODO: implement an iterator? this breaks encapsulation and relies on implementation --->
		<cfloop array="#B.members#" index="member">
			<cfif NOT this.isMember(member)>
				<cfreturn false>
			</cfif>
		</cfloop>
		<cfreturn true>
	</cffunction>

	<!--- For 2 sets to be considered equal, set A must be a subset of B and B must be a subset of A --->
	<cffunction name="equals" access="public" output="no" returntype="boolean">
		<cfargument name="B" type="set" required="yes">
		<cfreturn this.isSubset(B) AND B.isSubset(this)>
	</cffunction>

</cfcomponent>